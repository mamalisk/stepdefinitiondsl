package net.masterthought;

import org.openqa.selenium.WebDriver;

public class WebUser {

    private WebDriver browser;

    protected WebUser(WebDriver browser) {
        this.browser = browser;
    }

    public static WebUser whoUses(WebDriver browser){
        WebUser user = new WebUser(browser);
        return user;
    }

    public void closesTheBrowser() {
        browser.quit();
    }
}
