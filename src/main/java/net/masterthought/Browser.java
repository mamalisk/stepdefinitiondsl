package net.masterthought;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Browser {
    public static WebDriver firefox() {
        return new FirefoxDriver();
    }
}
