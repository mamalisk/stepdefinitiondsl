package net.masterthought;

public class Consumer extends DslCompatible {
    private WebUser user;
    private ShoppingItem item;

    protected Consumer(WebUser user){
       this.user = user;
    }

    public static Consumer isA(WebUser user){
        return new Consumer(user);
    }

    public Consumer and(Consumer consumer) {
        return consumer;
    }

    public Consumer addsToBasket(String item, String price){
       this.item = new ShoppingItem(item, Double.valueOf(price));
       return this;
    }

    public boolean verifiesThatHisPurchase() {
        return true;
    }

    public void closesTheBrowser() {
        user.closesTheBrowser();
    }

    public Consumer and(){
        return this;
    }


    public Consumer completesThePurchase() {
        return this;
    }
}
