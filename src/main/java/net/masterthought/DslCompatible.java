package net.masterthought;

import java.lang.reflect.Method;

public class DslCompatible {

    public static <T> T and(T t){
        return t;
    }

    public static <T> T is(T t){
        return t;
    }

    public static <T> T with(T t){
        return t;
    }


}
