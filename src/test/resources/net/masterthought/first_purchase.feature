Feature: First purchase for consumer
    As a merchant
    I want a new consumer to complete his first purchase successfully
    So that I can increase my clientele

Scenario: Consumer buys item
    Given I am a new consumer
    When I am ready to buy a 'book' for '11.99' GBP
    Then I can see that my purchase was successful
