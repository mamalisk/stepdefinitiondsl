package net.masterthought;

import cucumber.annotation.After;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.annotation.en.When;

import static net.masterthought.DslCompatible.with;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ConsumerTasks {
    private Consumer theConsumer;
    private final boolean successful = true;

    @After
    public void closeBrowser(){
        theConsumer.closesTheBrowser();
    }

    @Given("^I am a new consumer$")
    public void I_am_a_new_consumer() {
        theConsumer = Consumer.isA(WebUser.whoUses(Browser.firefox()));
    }

    @When("^I am ready to buy a '(.+)' for '(.+)' GBP$")
    public void I_am_ready_to_buy_a_book_for_GBP(String anItem, String price) {
        theConsumer.addsToBasket(anItem, with(price))
                   .and().completesThePurchase();
    }


    @Then("^I can see that my purchase was successful$")
    public void I_can_see_that_my_purchase_was_successful() {
        assertThat(theConsumer.verifiesThatHisPurchase(), is(successful));
    }

}
